%% Part #1  - System modeling
clear;clc;close

% Missile
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Flight point
alpha = 20*pi/180;
Mach  = 3;

% Other constants
g     = 9.80665;      % Standard gravity
a     = 316.0561; % Speed of sound @20000ft

% Compute airspeed
V     = Mach*a;

% Constants
Za = +1236.8918;
Ma = -300.4211;
Mq = +0;
Zd = +108.1144;
Md = -131.3944;
Aa = +1434.7783;
Ad = +115.0529;

% State space model
A_m = [-Za/V 1; Ma Mq];
B_m = [-Zd/V; Md];
C_m = [-Aa/g 0; 0 1];
D_m = [-Ad/g; 0];
G_m = ss(A_m,B_m,C_m,D_m);
G_m.InputName  = 'u_m';
G_m.StateName  = {'x1';'x2'};
G_m.OutputName = {'y1';'y2'};

% Actuator
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Constants
omega_a = 150;
zeta_a  = 0.7;

% State space model
A_a = [0 1; -omega_a^2 -2*zeta_a*omega_a];
B_a = [0; omega_a^2];
C_a = [1 0; 0 1];
D_a = [0; 0];
G_a = ss(A_a,B_a,C_a,D_a);
G_a.InputName  = 'u_cmd';
G_a.StateName  = {'x3';'x4'};
G_a.OutputName = {'u_m'; 'udot_m'};

% % Airframe
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
G_am_temp           = linearize('Airframe');
G_am_temp.StateName = {'x3';'x4';'x1';'x2'};
G_am                = ss2ss(G_am_temp,[0 0 1 0; 0 0 0 1; 1 0 0 0; 0 1 0 0]);

[n_G_am, d_G_am]    = ss2tf(G_am.A, G_am.B, G_am.C, G_am.D, 1);
G_am_nz             = zpk(tf(n_G_am(1,:), d_G_am));
G_am_q              = zpk(tf(n_G_am(2,:), d_G_am));

G_am
G_am_nz
G_am_q
% 
% % Analysis
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure("Name","Poles and zeros ol","NumberTitle","off"); 
subplot(1,3,1)
iopzmap(G_am(1,1))
damp(G_am(1,1))
subplot(1,3,2)
iopzmap(G_a)
subplot(1,3,3)
iopzmap(G_m)
% Step
figure();
subplot(1,2,1);
step(G_am(1,1));
subplot(1,2,2);
step(G_am(2,1));

% Save models
save G_a  G_a
save G_m  G_m
% save G_am G_am

G_am_1 = G_am;


%% Part #2  - Loop Shaping
% Question 2.1
figure("Name","RLocus plot to find C_q","NumberTitle","off"); 

G_ol_q = G_am_1(2,1);
rlocusplot(-G_ol_q);
% On en deduit pour amortissmement = 0.7
C_q = -0.163;

% % Airframe
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
G_cl_q_unsc_temp           = linearize('ClosedLoop_Cq');
G_cl_q_unsc_temp.StateName = {'x3';'x4';'x1';'x2'};
G_cl_q_unsc                = ss2ss(G_cl_q_unsc_temp,[0 0 1 0; 0 0 0 1; 1 0 0 0; 0 1 0 0]);

[n_G_cl_unsc_q, d_G_cl_unsc_q]    = ss2tf(G_cl_q_unsc.A, G_cl_q_unsc.B, G_cl_q_unsc.C, G_cl_q_unsc.D, 1);
G_cl_q_unsc_nz             = zpk(tf(n_G_cl_unsc_q(1,:), d_G_cl_unsc_q));

G_cl_q_unsc
G_cl_q_unsc_nz
% 
% % Analysis
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure("Name","Unscaled","NumberTitle","off"); 
iopzmap(G_cl_q_unsc(1,1))
damp(G_cl_q_unsc(1,1))

% Step
figure("Name","G_cl_q unscaled","NumberTitle","off"); 
step(G_cl_q_unsc(1,1));

% Question 2.2
G = dcgain(G_cl_q_unsc);
C_sc = 1/G;

% % Airframe
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
G_cl_q_temp           = linearize('ClosedLoop_CqCsc');
G_cl_q_temp.StateName = {'x3';'x4';'x1';'x2'};
G_cl_q                = ss2ss(G_cl_q_temp,[0 0 1 0; 0 0 0 1; 1 0 0 0; 0 1 0 0]);

[n_G_cl_q, d_G_cl_q]    = ss2tf(G_cl_q.A, G_cl_q.B, G_cl_q.C, G_cl_q.D, 1);
G_cl_q_nz             = zpk(tf(n_G_cl_q(1,:), d_G_cl_q));

G_cl_q
G_cl_q_nz


% Analyse
% Settling time 0.228
figure("Name","G_cl_q","NumberTitle","off"); 
step(G_cl_q(1,1));



% Question 2.3

C_i = 5.89;

% Linearisation
G_ol_q = linearize('ClosedLoop_CqCscCi');
%G_ol_q.StateName = {'x3';'x4';'x1';'x2';'integrator'};

[n_G_ol, d_G_ol]    = ss2tf(G_ol_q.A, G_ol_q.B, G_ol_q.C, G_ol_q.D, 1);
G_ol_nz             = zpk(tf(n_G_ol(1,:), d_G_ol));

G_ol_q
G_ol_nz

