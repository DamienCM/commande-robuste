%% Part #1  - System modeling
clear;clc;close

% Missile
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Flight point
alpha = 20*pi/180;
Mach  = 3;

% Other constants
g     = 9.80665;      % Standard gravity
a     = 316.0561; % Speed of sound @20000ft

% Compute airspeed
V     = Mach*a;

% Constants
Za = +1236.8918;
Ma = -300.4211;
Mq = +0;
Zd = +108.1144;
Md = -131.3944;
Aa = +1434.7783;
Ad = +115.0529;

% State space model
A_m = [-Za/V 1; Ma Mq];
B_m = [-Zd/V; Md];
C_m = [-Aa/g 0; 0 1];
D_m = [-Ad/g; 0];
G_m = ss(A_m,B_m,C_m,D_m);
G_m.InputName  = 'u_m';
G_m.StateName  = {'x1';'x2'};
G_m.OutputName = {'y1';'y2'};

% Actuator
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Constants
omega_a = 150;
zeta_a  = 0.7;

% State space model
A_a = [0 1; -omega_a^2 -2*zeta_a*omega_a];
B_a = [0; omega_a^2];
C_a = [1 0; 0 1];
D_a = [0; 0];
G_a = ss(A_a,B_a,C_a,D_a);
G_a.InputName  = 'u_cmd';
G_a.StateName  = {'x3';'x4'};
G_a.OutputName = {'u_m'; 'udot_m'};

% % Airframe
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
G_am_temp           = linearize('Airframe');
G_am_temp.StateName = {'x3';'x4';'x1';'x2'};
G_am                = ss2ss(G_am_temp,[0 0 1 0; 0 0 0 1; 1 0 0 0; 0 1 0 0]);

[n_G_am, d_G_am]    = ss2tf(G_am.A, G_am.B, G_am.C, G_am.D, 1);
G_am_nz             = zpk(tf(n_G_am(1,:), d_G_am));
G_am_q              = zpk(tf(n_G_am(2,:), d_G_am));

G_am
G_am_nz
G_am_q
% 
% % Analysis
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot(1,3,1)
iopzmap(G_am(1,1))
damp(G_am(1,1))
subplot(1,3,2)
iopzmap(G_a)
subplot(1,3,3)
iopzmap(G_m)


% Save models
save G_a  G_a
save G_m  G_m
% save G_am G_am

%% Part #2 - Weighting filters

% W1 (So)
dcgain_W1   = db2mag(-40);
freq_W1     = 6.5;
mag_W1      = db2mag(-3);
hfgain_W1   = 2;

% W2 (CSo)
dcgain_W2   = hfgain_W1;
freq_W2     = bandwidth(G_a(1,1));
mag_W2      = db2mag(-15);
hfgain_W2   = dcgain_W1;

% W3 (To)
dcgain_W3   = 1;
freq_W3     = bandwidth(G_a(1,1));
mag_W3      = db2mag(-35);
hfgain_W3   = dcgain_W1;

W1  = inv(makeweight(dcgain_W1,[freq_W1,mag_W1],hfgain_W1,0,1));
W2  = inv(makeweight(dcgain_W2,[freq_W2,mag_W2],hfgain_W2,0,1));
W3  = inv(makeweight(dcgain_W3,[freq_W3,mag_W3],hfgain_W3,0,2));

figure
sigma(inv(W1),'r',inv(W2),'b-',inv(W3),'g-')
grid on;xlim([1e-3 1e4])

save W1  W1
save W2  W2
save W3  W3